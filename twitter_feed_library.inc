<?php


function getTwitterTimeline() {

    // Credit:  https://www.codeofaninja.com/2015/08/display-twitter-feed-on-website.html

    // Params
    $count=3;
    $handle="cognosantellc";
    $oauth_access_token = "65052356-IdidnIHzuHfkfXcQUHF9UPV9qgsXM4GP4MHGwokbh";
    $oauth_access_token_secret = "MSmAKCVyUQ14TmxXpm4jxTAAcGhSyqJivVZrGhci4w3CQ";
    $consumer_key = "a6phLLXcVmdACZWraig8kX0HS";
    $consumer_secret = "tQt8DCMNyHTBD3v91LBDqCQs8WRDhXJnXROdYu0LJINGONQ12u";

    // we are going to use "user_timeline"
    $twitter_timeline = "user_timeline";

    // specify number of tweets to be shown and twitter username
    // for example, we want to show 20 of Taylor Swift's twitter posts
    $request = array(
        'count' => $count,
        'screen_name' => $handle
    );


    // put oauth values in one oauth array variable
    $oauth = array(
        'oauth_consumer_key' => $consumer_key,
        'oauth_nonce' => time(),
        'oauth_signature_method' => 'HMAC-SHA1',
        'oauth_token' => $oauth_access_token,
        'oauth_timestamp' => time(),
        'oauth_version' => '1.0'
    );

    // combine request and oauth in one array
    $oauth = array_merge($oauth, $request);

    // make base string
    $baseURI="https://api.twitter.com/1.1/statuses/$twitter_timeline.json";
    $method="GET";
    $params=$oauth;

    $r = array();
    ksort($params);
    foreach($params as $key=>$value){
        $r[] = "$key=" . rawurlencode($value);
    }
    $base_info = $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);

    // get oauth signature
    $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
    $oauth['oauth_signature'] = $oauth_signature;

    // make request
    // make auth header
    $r = 'Authorization: OAuth ';

    $values = array();
    foreach($oauth as $key=>$value){
        $values[] = "$key=\"" . rawurlencode($value) . "\"";
    }
    $r .= implode(', ', $values);

    // get auth header
    $header = array($r, 'Expect:');

    // set cURL options
    $options = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_HEADER => false,
        CURLOPT_URL => "https://api.twitter.com/1.1/statuses/$twitter_timeline.json?". http_build_query($request),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => true
    );

    // retrieve the twitter feed
    $feed = curl_init();
    curl_setopt_array($feed, $options);
    $json = curl_exec($feed);
    curl_close($feed);

    // decode json format tweets
    $tweets=json_decode($json, true);

    // Add tweet html representation
    $url_regex = "/(?<!a href=\")(?<!src=\")((http|ftp)+(s)?:\/\/[^<>\s]+)/i"; // Replace urls with hyperlinks
    $hash_regex = "/#+([a-zA-Z0-9_]+)/"; // Replace hashtags with hyperlinks
    $at_regex = "/@+([a-zA-Z0-9_]+)/"; // Replace at tags with hyperlinks
    foreach($tweets as $key=>$tweet) {
        $tweethtml = $tweet['text'];
        $tweethtml = preg_replace($url_regex, '<a target="_blank" href="$0">$0</a>', $tweethtml);
        $tweethtml = preg_replace($hash_regex, '<a target="_blank" href="https://twitter.com/search?q=%23$1">$0</a>', $tweethtml);
        $tweethtml = preg_replace($at_regex, '<a target="_blank" href="https://twitter.com/$1">$0</a>', $tweethtml);
        $tweets[$key]['html']=$tweethtml;
    }

    // Add tweet url
    foreach($tweets as $key=>$tweet) {
        // https://twitter.com/CognosanteLLC/status/783744142589497344
        $tweets[$key]['url']= "https://twitter.com/$handle/status/".$tweet['id_str'];
    }

    return $tweets;
}

function getTwitterHtmlFeed() {
    $timeline = getTwitterTimeline();
    $feed = [];
    foreach($timeline as $tweet) {
        $timestamp = strtotime($tweet['created_at']);
        $feed[$timestamp]=[
            "html"=>$tweet['html'],
            "prettytime"=>prettifyTimestamp($timestamp),
            "timestamp"=>$timestamp,
            "url"=>$tweet['url']
        ];
    }
    sort($feed);
    return $feed;
}

function prettifyTimestamp($timestamp) {
    $now = (new DateTime(null, new DateTimeZone('America/New_York')))->getTimeStamp();
    $delta = floor(($now - $timestamp)/60);
    if ($delta < 60) {
        $prettytime = $delta."m ago";
    } else if ($delta < 1440) {
        $prettytime = floor($delta/60)."h ago";
    } else {
        $prettytime = date("M j", $timestamp);
    }
    return $prettytime;
}

function getTwitterJsonFeed() {
    $feed = getTwitterHtmlFeed();
    return json_encode($feed);
}

function serveHtml() {
    $feed = getTwitterHtmlFeed();
    header("HTTP/1.1 200 OK");
    foreach($feed as $timestamp=>$tweet) {
        print "<a href='".$tweet['url']."' >".$tweet['prettytime']."</a>: ".$tweet['html']."<br>";
    }
}

function serveAjax() {
    $js = getTwitterJsonFeed();
    header("HTTP/1.1 200 OK");
    header('Content-Type: application/json');
    print $js;
}

//serveHtml();
//serveAjax();

?>