# Simple Twitter Feed Drupal Module #

This Drupal module creates an ajax endpoint '/simpletwitterfeed' to get the latest 3 tweets from a twitter handle.  The twitter credentials and which handle to use are configured in twitter_feed_library.inc.

Note:  Twitter requests registering each website and updating the credentials.