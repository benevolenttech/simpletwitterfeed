<?php

namespace Drupal\simpletwitterfeed\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\DataCommand;

// Note:  if including a php file just change 'inc' to php
module_load_include('inc', 'simpletwitterfeed','twitter_feed_library');

/**
 * SimpletwitterfeedController controller.
 */
class SimpletwitterfeedController extends ControllerBase
{

    /**
     * {@inheritdoc}
     */
    public function content()
    {
        #$build = array(
        #    '#type' => 'markup',
        #    '#markup' => t('Hello World!'),
        #);
        #return $build;

        $response = new AjaxResponse();
        $twitter_json = getTwitterJsonFeed();
        $response->addCommand(new DataCommand('tweetdiv',  'json_data', $twitter_json));
        return $response;
    }

}