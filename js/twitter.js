
// Will replace #tweetdiv html with a twitter feed
$(document).ready(function() {
    $.ajax({url: "/simpletwitterfeed", success: function(result){
        var feedJson = JSON.parse(result[0].value);
        var twitterfeed_html = "";
        Object.keys(feedJson).forEach(function(timestamp) {
            var t = feedJson[timestamp];
            twitterfeed_html += "<p>"+t.prettytime+" - <em>"+t.html+"</em></p>";
        });
        $('#tweetdiv').html(twitterfeed_html);
    }});
})
